# KYGnus Guard MSwindows

**KYGnus Guard MSwindows** Antivirus is a part of my Antivirus project that developed after the Linux version. There were several basic goals in the development of this antivirus:
- **Familiarity of Windows users with Linux environment and Linux thinking**
    - The first issue in this section is related to working with the command line,
      because the use of the graphical environment is very boring and the unnecessary use of system resources 
      and complexity in the work. For this reason, this antivirus runs in the commandline environment.
    - 

- **Freedom of action in changes to the Software**
    - The second theme in this regard is the use of a free and changeable database that any user or organization 
      can create a unique database or discover their own dangerous files and place it inside the database in order to use it.
      So that file is known as a virus
    - All the settings in Linux are in text files and you have to modify the text files to change the software settings.
     In this software, the setting file is a text file and you can open and modify it with a text editor.

- **Being Portable**
    - This software does not need to be installed and you can scan all the computers
     in your home or organization by copying the file to a CD or USB disk.

- **Simplicity of SETTING and USE**
    - You will not need advanced or difficult settings on your antivirus so that the software can do its normal work.
     You will be able to launch the software easily
    


## Download

### CMD

curl -O https://gitlab.com/KooshaYeganeh/kygnus-guard-mswindows/-/archive/main/kygnus-guard-mswindows-main.zip


### Direct Download

[Download](https://gitlab.com/KooshaYeganeh/kygnus-guard-mswindows/-/archive/main/kygnus-guard-mswindows-main.zip)



## Install


**1-** unzip kygnus-guard-mswindows-main.zip and put all Files in Directory in Same Folder  
**2-** Open CMD in Administrator mode and go to the path where you placed the Files  
**3-** run exe File

**or**

**1-** Go to the path of the files  
**2-** Open the exe file in Administrator mode




## Contact 


**website** : KooshaYeganeh.github.io  
**GitHub** : https://github.com/KooshaYeganeh  
**GitBook** : Koosha  
**Gmail** : kooshakooshadv@gmail.com  
**ProtonMail** : kooshakooshadv@protonmail.com  



## Reource 

https://urlhaus.abuse.ch/browse/



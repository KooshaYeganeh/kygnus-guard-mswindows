@echo off
setlocal enabledelayedexpansion

set "target_folder=C:\"  :: Change this to your desired folder

echo Removing hidden .exe files from %target_folder%...

for /r "%target_folder%" %%f in (*.exe) do (
    attrib "%%f" | find "H" > nul
    if !errorlevel! equ 0 (
        echo Removing hidden file: %%f
        attrib -h "%%f"
        del /f "%%f"
    )
)

echo Removal complete.
pause


@echo off
setlocal enabledelayedexpansion

set "target_folder=C:\"  :: Change this to your desired folder

echo Searching for hidden files in %target_folder%...

for /r "%target_folder%" %%f in (*) do (
    attrib "%%f" | find "H" > nul
    if !errorlevel! equ 0 (
        echo Hidden file found: %%f
    )
)

echo Search complete.
pause


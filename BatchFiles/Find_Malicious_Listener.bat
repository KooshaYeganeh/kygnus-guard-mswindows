@echo off

echo Searching for malicious listeners...

REM Get active network connections
netstat -ano > connections.txt

REM Find listening ports and associated processes
findstr "LISTENING" connections.txt > listeners.txt

REM Extract the process IDs (PIDs) of the listening ports
for /f "tokens=5" %%a in (listeners.txt) do (
    set "pid=%%a"
    echo Checking PID !pid!...
    REM Check if the process is suspicious (modify as needed)
    tasklist /fi "PID eq !pid!" | findstr /i /r "malware.exe virus.exe" > nul
    if %errorlevel% equ 0 (
        echo Suspicious listener found! PID: !pid!
    )
)

REM Clean up temporary files
del connections.txt listeners.txt

echo Search complete.
pause

